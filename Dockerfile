# syntax=docker/dockerfile:1
FROM zxvcv/conan:latest
WORKDIR /app

ARG DOWNLOADS=/temp/download
ARG INSTALL=/usr/share

ARG GCC_ARM_VERSION=10.3-2021.10
ARG GCC_ARM_DOWNLOAD=https://armkeil.blob.core.windows.net/developer/Files/downloads/gnu-rm

# install gcc-arm-none-eabi
ARG GCC_ARM=gcc-arm-none-eabi
ARG GCC_ARM_FILE=${GCC_ARM}-${GCC_ARM_VERSION}-x86_64-linux.tar.bz2
RUN curl ${GCC_ARM_DOWNLOAD}/${GCC_ARM_VERSION}/${GCC_ARM_FILE} --create-dirs -o ${DOWNLOADS}/${GCC_ARM_FILE} \
 && tar -xjf ${DOWNLOADS}/${GCC_ARM_FILE} -C ${INSTALL} \
 && ln -s /usr/share/${GCC_ARM}-${GCC_ARM_VERSION}/bin/arm-none-eabi-* /usr/bin \
 && rm -r ${DOWNLOADS}
