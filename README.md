zxvcv/gcc-arm-none-eabi
=======================
Image created mainly to work with the gcc-arm-none-eabi compiler
and to work with conan packages for ARM-based microcontrollers.

Run docker image in interactive mode
------------------------------------
```
docker run -ti zxvcv/gcc-arm-none-eabi:latest
```

Run docker images in interactive mode as current user:
```
docker run -ti -u $(id -u ${USER}):$(id -g ${USER}) zxvcv/gcc-arm-none-eabi:latest
```

Pushing docker image to dockerhub
---------------------------------
Dockerhub authentication: `docker login`
Push image to dockerhub: `docker push zxvcv/gcc-arm-none-eabi:latest`

Helps
-----
```
service docker start
sudo docker run -ti -w=/home/conan/pckg -v ${HOME}/.conan:/root/.conan -v $(pwd):/home/conan/pckg zxvcv/gcc-arm-none-eabi:latest
docker build -t zxvcv/gcc-arm-none-eabi:latest
```

References:
-----------
- Repository: https://gitlab.com/zxvcv-docker/gcc-arm-none-eabi
- Docker Hub: https://hub.docker.com/repository/docker/zxvcv/gcc-arm-none-eabi
